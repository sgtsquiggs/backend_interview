# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('line1', models.CharField(max_length=255)),
                ('line2', models.CharField(max_length=255, null=True, blank=True)),
                ('city', models.CharField(max_length=255)),
                ('state', models.CharField(max_length=255, null=True, blank=True)),
                ('postal_code', models.CharField(max_length=20, null=True, blank=True)),
                ('country', django_countries.fields.CountryField(max_length=2)),
                ('notes', models.TextField(null=True)),
                ('status', models.CharField(default=b'active', max_length=20, choices=[(b'active', b'Active'), (b'deleted', b'Deleted')])),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
