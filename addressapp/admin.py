from django.contrib import admin
from addressapp.models import Address

# Register your models here.
admin.site.register(Address)
