from django.conf import settings
from django.db import models
from django_countries.fields import CountryField

# Create your models here.

class Address(models.Model):
    STATUSES = (
        ('active', 'Active',),
        ('deleted', 'Deleted',),
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    line1 = models.CharField(max_length=255)
    line2 = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=255)
    state = models.CharField(max_length=255, null=True, blank=True)
    postal_code = models.CharField(max_length=20, null=True, blank=True)
    country = CountryField()
    notes = models.TextField(null=True)
    status = models.CharField(max_length=20, choices=STATUSES, default='active')
    created = models.DateTimeField(auto_now_add=True, null=False)
    updated = models.DateTimeField(auto_now=True)
